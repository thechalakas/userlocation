package com.thechalakas.jay.userlocation;

/*
 * Created by jay on 27/09/17. 12:25 PM
 * https://www.linkedin.com/in/thesanguinetrainer/
 */

import android.*;
import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.instantapps.ActivityCompat;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.*;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;

public class MainActivity extends FragmentActivity
{

    private static final int PERMISSION_RESULT_LOCATION = 0;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationManager locationManager;
    private String provider;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.i("onCreate","onCreate Reached");

        LocationManager service = (LocationManager) getSystemService(LOCATION_SERVICE);
        boolean enabled = service.isProviderEnabled(LocationManager.GPS_PROVIDER);
        Log.i("onCreate","Enabled is - "+enabled);

        LocationStuff locationstuff = new LocationStuff();
        // check if enabled and if not send user to the GSP settings
        // Better solution would be to display a dialog and suggesting to
        // go to the settings
        if (!enabled)
        {
            Log.i("onCreate","GPS settings");
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent);
        }

        // Get the location manager
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        // Define the criteria how to select the locatioin provider -> use
        // default
        Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, false);

        int permissionCheck = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION);

        Log.i("onCreate","permissionCheck is " +permissionCheck);

        try {
             requestPermissions(new String[] {Manifest.permission.ACCESS_FINE_LOCATION},PERMISSION_RESULT_LOCATION);
            Log.i("onCreate","permission result is " +PERMISSION_RESULT_LOCATION);


        } catch (Exception e) {
            e.printStackTrace();
        }

        if (permissionCheck == PackageManager.PERMISSION_GRANTED)
        {
            Log.i("onCreate","PackageManager.PERMISSION_GRANTED");
            Location location = locationManager.getLastKnownLocation(provider);

            // Initialize the location fields
            if (location != null)
            {
                Log.i("onCreate","location is not null");
                //onLocationChanged(location);
                locationstuff.onLocationChanged(location);
            }
            else
            {
                Log.i("onCreate","location is  null");
                //latituteField.setText("Location not available");
                //longitudeField.setText("Location not available");
            }
        }
        else
        {
            Log.i("onCreate","PackageManager.PERMISSION_GRANTED is false");
        }


    }//end of OnCreate

    private class LocationStuff implements LocationListener
    {
        @Override
        public void onLocationChanged(Location location)
        {
            int lat = (int) (location.getLatitude());
            int lng = (int) (location.getLongitude());

            Log.i("onCreate","onLocationChanged - Latitude - " + lat);
            Log.i("onCreate","onLocationChanged - Longitude - " + lng);

        }


    }

    private class MapStuff implements OnMapReadyCallback
    {
        private GoogleMap mMap;

        @Override
        public void onMapReady(GoogleMap googleMap)
        {
            Log.i("onCreate","MapStuff - onMapReady");


            mMap = googleMap;

            // Add a marker in Sydney and move the camera
            LatLng sydney = new LatLng(-34, 151);
            mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));


            // Add a marker in Sydney and move the camera
            LatLng sydney2 = new LatLng(-32.5, 151.5);
            mMap.addMarker(new MarkerOptions().position(sydney2).title("Marker2 in Sydney"));
            //mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

            // Add a marker in Sydney and move the camera
            LatLng sydney3 = new LatLng(-30.8, 151.9);
            mMap.addMarker(new MarkerOptions().position(sydney3).title("Marker3 in Sydney"));
            //mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        }
    }

    @Override
    public void onRequestPermissionsResult(
            int requestCode,
            String permissions[],
            int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_RESULT_LOCATION:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    //Toast.makeText(MainActivity.this, "Permission Granted!", Toast.LENGTH_SHORT).show();
                    Log.i("onCreate","Permission Granted!");
                }
                else {
                    //Toast.makeText(MainActivity.this, "Permission Denied!", Toast.LENGTH_SHORT).show();
                    Log.i("onCreate","Permission Denied!");
                }
        }
    }

}


